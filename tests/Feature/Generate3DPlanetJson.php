<?php

namespace Tests\Feature;

use App\Locationbase;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Geotools\Coordinate\Coordinate;
use Tests\TestCase;

class Generate3DPlanetJson extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testJson()
    {
        $placesJson = json_decode('{"type": "FeatureCollection","features": []}');

        $sites = Locationbase::where('lat', '<>', '')->where('lng', '<>', '')->get();

        foreach($sites as $site) {
            $feature = json_decode('{ "type": "Feature", "properties": { "scalerank": 0, "labelrank": 1, "featurecla": "Populated place", "name": "Los Angeles", "nameascii": "Los Angeles", "adm0name": "United States of America", "adm0_a3": "USA", "adm1name": "California", "iso_a2": "US", "note": null, "latitude": 13.989978250199997, "longitude": -118.179980511, "changed": 0.0, "namediff": 0, "diffnote": null, "pop_max": 12500000, "pop_min": 3694820, "pop_other": 142265, "rank_max": 14, "rank_min": 12, "geonameid": 5368361.0, "meganame": "Los Angeles-Long Beach-Santa Ana", "ls_name": "Los Angeles1", "ls_match": 1, "checkme": 0 }, "geometry": { "type": "Point", "coordinates": [ -118.181926369940413, 33.991924108765431 ] } }');
            $feature->geometry->coordinates = [$site->lat, $site->lng];

            $placesJson->features[] = $feature;
        }

        Storage::put('places.json', json_encode($placesJson), 'public');
    }

}
