<?php

namespace Tests\Feature;

use App\Mission;
use App\Payload;
use App\Rocket;
use App\Site;
use League\Csv\Reader;
use PHPHtmlParser\Dom;
use Tests\TestCase;

class ImportSpaceFlights extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testImportSkyRocketDe()
    {
        $htmlFile = storage_path('app'.DIRECTORY_SEPARATOR.'scraps').DIRECTORY_SEPARATOR.'space_flight.html';

        $records = preg_split('/'.str_repeat(PHP_EOL,2).'/', file_get_contents($htmlFile));

        $existingSites = [];
        $existingRockets = [];

        foreach($records as $record) {

            //dd($record);

            // SITE
            $siteCountry = $this->parse($record, '/PAYS.+?>(.+?)</');
            $siteName = $this->parseSiteName($record, '/SPATIAL.+?title="(.+?)"/');
            if(isset($existingSites[$siteCountry.$siteName])) {
                $site = $existingSites[$siteCountry.$siteName];
            }
            else {
                $site = new Site();
                $site->country = $siteCountry;
                $this->assertNotEmpty($site->country);
                $site->name = $siteName;
                //$this->assertNotEmpty($site->name, $record);
                $site->save();
                $existingSites[$siteCountry.$siteName] = $site;
            }


            // ROCKET
            $rocketName = $this->parse($record, '/type3">(.+?)<\/a>.+?VOL:/');
            if(isset($existingRockets[$rocketName])) {
                $rocket = $existingRockets[$rocketName];
            }
            else {
                $rocket = new Rocket();
                $rocket->name = $rocketName;
                //$this->assertNotEmpty($rocket->name, json_encode($record));
                $rocket->save();
                $existingRockets[$rocketName] = $rocket;
            }


            // MISSION
            $mission = new Mission();
            $mission->name = $this->parse($record, '/<h1>(.+?)<\/h1>/');
            $mission->video = $this->parse($record, '/<iframe .*?src="(.+?)"/');
            $mission->status = $this->parse($record, '/RESULTAT.+?b>(.+?)<\/p>/');
            $mission->flight_number = $this->parse($record, '/RESULTAT.+?b>(.+?)<\/p>/');
            $mission->pas_de_tir = $this->parse($record, '/PAS DE TIR: .+?b>(.+?)<br>/');
            $this->assertNotEmpty($mission->name);
            if($dateParts = $this->parse($record, '/DATE: <\/b>(.+?) à (.+?) TU<br>/')) {
                $mission->launching_at = trim($this->parseDate($dateParts[0]).' '.$this->parseTime($dateParts[1]));
                $this->assertNotNull($mission->launching_at, json_encode($dateParts));
                $this->assertRegExp('/[0-9]{4}-[0-9]{2}-[0-9]{2}( [0-9]{2}:[0-9]{2}:[0-9]{2})?/', $mission->launching_at, json_encode($dateParts));
            }
            $mission->rocket_id = $rocket->id;
            $mission->launching_site_id = $site->id;
            $mission->save();


            // PAYLOADS
            if($payloads = $this->parse($record, '/CHARGE UTILE.+?<br>(.+?)<br><b>/')) {
                $payloads = array_filter(explode('<br>', $payloads));

                // A tester
                //$this->assertNotEmpty($payloads);

                foreach($payloads as $payloadName) {
                    $this->assertNotEmpty($payloadName);

                    $payload = new Payload();
                    $payload->name = $payloadName;
                    $payload->mission_id = $mission->id;
                    $payload->save();
                }
            }
        }
    }

    private function parse($record, $regExp)
    {
        preg_match($regExp, $record, $results);
        array_shift($results);

        return !isset($results[0]) ? null : (count($results) === 1 ? $results[0] : $results);
    }

    private function parseDate($date)
    {
        return implode(
            '-',
            array_reverse(
                explode('-', $date)
            )
        );
    }

    private function parseTime($time)
    {
        if(strpos($time, '?') !== false) {
            return null;
        }

        $timeParts = explode(':', $time);

        foreach($timeParts as &$timePart) {
            $timePart = str_pad($timePart, 2, '0');
        }

        return implode(':', $timeParts);
    }

    private function parseSiteName($record, $regExp)
    {
        $name = $this->parse($record, $regExp);

        /*if(strpos($name, 'Cosmodrome de ') !== false) {
            $name = preg_replace('/Cosmodrome de (.+)$/', '$1 Space Center', $name);
        }*/

        return $name;
    }
}
