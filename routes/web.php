<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/countdown', function () {
    return view('countdown');
});
Route::get('/info', function () {
    return view('infographie');
});
Route::get('/map', function () {
    return view('flatMap');
});
Route::get('/scatter', 'HomeController@scatter');
Route::get('/timeline', 'HomeController@timeline');
