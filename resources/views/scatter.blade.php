@extends('layout' )

@section('content')
    <section data-transition="slide" style="background: #fff;">
        <div class="wraper">

            <script src="js/Chart.bundle.js"></script>
            <script src="js/utils.js"></script>
            <style>
                canvas {
                    -moz-user-select: none;
                    -webkit-user-select: none;
                    -ms-user-select: none;
                }
            </style>
            <div style="width:75%">
                <canvas id="canvas"></canvas>
            </div>
            {{-- <button id="randomizeData">Randomize Data</button> --}}
            <script>
                var color = Chart.helpers.color;
                var scatterChartData = {
                    datasets: [
                        {
                            label: 'Numbers of launch of rockets by years',
                            borderColor: window.chartColors.red,
                            backgroundColor: color(window.chartColors.red).alpha(0.2).rgbString(),
                            data: {!! $points !!},
                        }
                    ]
                };

                window.onload = function () {
                    var ctx = document.getElementById('canvas').getContext('2d');
                    window.myScatter = Chart.Scatter(ctx, {
                        data: scatterChartData,
                        options: {
                            title: {
                                display: true,
                                text: 'Chart.js Scatter Chart'
                            },
                        }
                    });
                };

                document.getElementById('randomizeData').addEventListener('click', function () {
                    scatterChartData.datasets.forEach(function (dataset) {
                        dataset.data = dataset.data.map(function () {
                            return {
                                x: randomScalingFactor(),
                                y: randomScalingFactor()
                            };
                        });
                    });
                    window.myScatter.update();
                });
            </script>
        </div>
    </section>

@endsection