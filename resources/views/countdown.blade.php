@extends('layout' )

@section('styles')
    <link rel="stylesheet" href="css/reveal.css">
@endsection
@section('scripts')
    @include('partials.js-reveal')
@endsection

@section('content')
    <!-- Countdown -->
    <section data-transition="slide"
             data-background-image="lib/images/launch1.jpg"
             data-background-size="cover">
        <div class="wraper">
            <h2>Next mission in</h2>
            <div class="countdown" id="countdownLink">
                <ul>
                    <li><span id="days"></span>days</li>
                    <li><span id="hours"></span>Hours</li>
                    <li><span id="minutes"></span>Minutes</li>
                    <li><span id="seconds"></span>Seconds</li>
                </ul>
            </div>{{--
                    <p>Location</p>
                    <p><small>Sub-informations</small></p>--}}
            <br>
            <a href="#" class="resume-button">
                Être averti des prochains lancements
            </a>
        </div>
    </section>
@endsection
