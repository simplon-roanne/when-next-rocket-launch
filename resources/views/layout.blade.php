<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>SpaceApp - countdown & timeline</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="css/theme/black.css">
    @yield('styles')
    <link rel="stylesheet" href="lib/css/styles.css">

    <script src="lib/js/head.min.js"></script>
</head>

<body>
    <div class="reveal">
        @include('partials/header')
        <div class="slides">
            <!--<div class="borders">-->
            <!--<div class="top"></div>-->
            <!--<div class="right"></div>-->
            <!--<div class="bottom"></div>-->
            <!--<div class="left"></div>-->
            <!--</div>-->
            @yield('content')
        </div>
    </div>
    <script src="lib/js/countdown.js"></script>
    @yield('scripts')
</body>
</html>
