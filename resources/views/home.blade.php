@extends('layout' )

@section('styles')
    <link rel="stylesheet" href="css/reveal.css">
@endsection
@section('scripts')
    @include('partials.js-reveal')
@endsection

@section('content')

    <div class="slides">

        <section data-transition="slide"
                 data-background-image="lib/images/launch2.jpg"
                 data-background-size="cover">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="wrapper col-6" style="width: 50%;display: inline-block; position: absolute;left: 0;">
                        @include('earth')
                    </div>
                    <div class="col-6"
                         style="width: 50%; font-family: 'Montserrat', sans-serif; left: 50%; position: absolute; font-size: 100px; display: inline-block">
                        <br>The World<br> Is a Launching<br> Ground !
                    </div>
                </div>
            </div>


        </section>
        <!-- Countdown -->
        <section data-transition="slide"
                 data-background-image="lib/images/launch1.jpg"
                 data-background-size="cover">
            <div class="wraper">
                <h2>Next mission in</h2>
                <div class="countdown" id="countdownLink">
                    <ul>
                        <li><span id="days"></span>days</li>
                        <li><span id="hours"></span>Hours</li>
                        <li><span id="minutes"></span>Minutes</li>
                        <li><span id="seconds"></span>Seconds</li>
                    </ul>
                </div>{{--
                    <p>Location</p>
                    <p><small>Sub-informations</small></p>--}}
                <br>
                <a href="#" class="resume-button">
                    Être averti des prochains lancements
                </a>
            </div>
        </section>

    </div>
@endsection

