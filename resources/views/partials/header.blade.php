<header>
    <div style="padding: 5px 5px 5px 5px;">
        <img src="lib/images/sal_logo_white_sm.png" width="70px" style="display: inline-block; float: left;">
    </div>
    <a href="countdown" style="color: white">
        <div class="countdown" style="font-size: 1em; float: left; display: inline-block;    line-height: 20px;">
            <small style="display: block;font-size: 12px;">Prochain lancement :</small>
            <ul style="border-top: 0px solid rgba(255,255,255,0)!important;border-bottom: 0px solid rgba(255,255,255,0)!important;    line-height: 16px;">
                <li><span style="font-size: 1em; display: inline-block;" id="days2"> </span> D</li>
                <li><span style="font-size: 1em; display: inline-block;" id="hours2"> </span> H</li>
                <li><span style="font-size: 1em; display: inline-block;" id="minutes2"> </span> M</li>
                <li><span style="font-size: 1em; display: inline-block;" id="seconds2"> </span> S</li>
            </ul>
        </div>
    </a>
    <a href="{{ url('/') }}">
        <img src="lib/images/rocket-data-logo.png" style="position: absolute;top: 10px;left: 50%;margin-left: -59px;">
    </a>
    <nav>
        <a href="info">Infographie</a>&nbsp;&nbsp;
        <a href="scatter">Graphique</a>&nbsp;&nbsp;
        <a href="map">Carte</a>&nbsp;&nbsp;
        <a href="timeline">Timeline</a>
    </nav>
</header>