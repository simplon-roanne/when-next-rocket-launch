@extends('layout' )

@section('styles')
    <link rel="stylesheet" href="css/reveal.css">
@endsection
@section('scripts')
    @include('partials.js-reveal')
@endsection

@section('content')
    <section>
        <!-- Timeline -->
        @foreach($missions as $i => $mission)
            <section data-transition="slide"
                     data-background-image="lib/images/launch{{rand(1,5)}}.jpg"
                     data-background-size="cover">
                <div class="wrapper">
                    <div class="grid timeline">
                        <div class="col12 timeline-meta">
                            <p>{{ $mission->launching_at }} - Mission {{ $mission->name }}</p>
                            <h3>{{$mission->site->name}}</h3>
                            <h4 style="padding-left: 60px;">Résultat : {{$mission->flight_number}}</h4>
                        </div>
                        <div class="col3 card" style="margin-right: 10px;">
                            <div class="card-body">
                                <h5 class="card-title">Lanceur</h5>
                                <p class="card-text">
                                    {{$mission->rocket->name}}<br>
                                    Pas de tir : {{$mission->pas_de_tir}}
                                </p>
                                <a href="#" class="card-link">+ d'infos</a>
                            </div>
                        </div>
                        <div class="col1"></div>
                        <div class="col3 card" style="margin-right: 10px;">
                            <div class="card-body">
                                <h5 class="card-title">Charge Utile</h5>
                                <p class="card-text">
                                    @foreach($mission->payloads as $payload)
                                        {{ $payload->name }}<br>
                                    @endforeach
                                </p>
                                <a href="#" class="card-link">+ d'infos</a>
                            </div>
                        </div>
                        @if($mission->video)
                            <div class="col4 card">
                                <div class="card-body">
                                    <h5 class="card-title">En vidéo</h5>
                                    <table class="card-table">
                                        <iframe id="player" type="text/html" width="360" height="200"
                                                src="{{$mission->video}}" frameborder="0"></iframe>
                                    </table>
                                </div>
                            </div>
                        @endif
                    </div>
                    <a href="#" class="navigate-down resume-button">
                        Previous mission
                    </a>
                </div>
            </section>
        @endforeach
    </section>
@endsection