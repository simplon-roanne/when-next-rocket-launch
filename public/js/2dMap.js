var CapeCanaveralAirStation = new ol.geom.Circle(
    //756 lancés 13
    ol.proj.transform([-80.5728,28.4886], 'EPSG:4326', 'EPSG:3857'),
    100000*13
);
var CosmodromeDeBaikonour = new ol.geom.Circle(
    //1489 lancés 26
    ol.proj.transform([63.3052,45.9645], 'EPSG:4326', 'EPSG:3857'),
    100000*26
);
var VandenbergAirForceBase = new ol.geom.Circle(
    //698 lancés 12
    ol.proj.transform([-120.5163,34.75821], 'EPSG:4326', 'EPSG:3857'),
    100000*12
);
var CosmodromeDePlesetsk = new ol.geom.Circle(
    //1623 lancés 28
    ol.proj.transform([40.574841,62.927855], 'EPSG:4326', 'EPSG:3857'),
    100000*28
);
var SanMarcoPlateForm = new ol.geom.Circle(
    //9 lancés 0,1
    ol.proj.transform([40.194845,-2.995738], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var JiuquanSpaceCenter = new ol.geom.Circle(
    //108 lancés 1,9
    ol.proj.transform([94.66238,40.129998], 'EPSG:4326', 'EPSG:3857'),
    100000*1.9
);
var XichangSpaceCenter = new ol.geom.Circle(
    //121 lancés 2,1
    ol.proj.transform([102.244872,27.893082], 'EPSG:4326', 'EPSG:3857'),
    100000*2.1
);
var SatishDhawanSpaceCenter = new ol.geom.Circle(
    //65 lancés 1,1
    ol.proj.transform([80.226555,13.725865 ], 'EPSG:4326', 'EPSG:3857'),
    100000*1.1
);
var KennedySpaceCenter = new ol.geom.Circle(
    // 165 lancés 2,8
    ol.proj.transform([-80.648981,28.572872], 'EPSG:4326', 'EPSG:3857'),
    100000*2.8
);
var CentreSpatialGuyanais = new ol.geom.Circle(
    // 283 lancés 4,9
    ol.proj.transform([-52.788417,5.04285 ], 'EPSG:4326', 'EPSG:3857'),
    100000*4.9
);
var CosmodromeDeKapustinYar = new ol.geom.Circle(
    // 102 lancés 1,8
    ol.proj.transform([45.766175,48.575267], 'EPSG:4326', 'EPSG:3857'),
    100000*1.8
);
var TaiyuanSatelliteLaunchCenter = new ol.geom.Circle(
    //69 lancés 1,2
    ol.proj.transform([112.548879,37.87059], 'EPSG:4326', 'EPSG:3857'),
    100000*1.2
);
var OceanPacifique = new ol.geom.Circle(
    //36 lancés 0,6
    ol.proj.transform([-120.508523, -8.783195], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var UchinouraSpaceCenter = new ol.geom.Circle(
    //40 lancés 0,7
    ol.proj.transform([131.076111,31.251236], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var WallopsFlightFacility = new ol.geom.Circle(
    //27 lancés 0,5
    ol.proj.transform([-75.469884,37.936702], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var TanegashimaSpaceCenter = new ol.geom.Circle(
    // 78 lancés 1,3
    ol.proj.transform([130.957453,30.37474], 'EPSG:4326', 'EPSG:3857'),
    100000*1.3
);
var CentroDeLacamentoDeAlcantara = new ol.geom.Circle(
    //3 lancés 0,05
    ol.proj.transform([-44.417145,-2.338365], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var MidAtlanticRegionalSpaceport = new ol.geom.Circle(
    //
    ol.proj.transform([-75.471112,37.850521], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var NaroSpaceCenter = new ol.geom.Circle(
    // 3 lancés 0,1
    ol.proj.transform([127.534111,34.44173], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var CosmodromeDeDombarovsky = new ol.geom.Circle(
    // 10 lancés 0,5
    ol.proj.transform([59.842401,51.094366], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var GandoAirBase = new ol.geom.Circle(
    // 1 lancé 0,1
    ol.proj.transform([-15.387707,27.933185], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var PalmachimAirBase = new ol.geom.Circle(
    // 10 lancé 0,5
    ol.proj.transform([34.68018,31.884686], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var EdwardsAirForceBase = new ol.geom.Circle(
    //5 lancés 0,1
    ol.proj.transform([-117.891208,34.924031], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var TonghaeSatelliteLaunchingGround = new ol.geom.Circle(
    // 2 lancés 0,1
    ol.proj.transform([127.510093,40.339852], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var RonaldReaganBallisticMissileDefenseTestSite = new ol.geom.Circle(
    // 9 lancés 0,1
    ol.proj.transform([-123.508523, -8.283195], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var ChinaLakeAirForceBase = new ol.geom.Circle(
    // 5 lancés 0,1
    ol.proj.transform([-117.657437,35.655171], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var WoomeraRocketRange = new ol.geom.Circle(
    // 6 lancés 0,1
    ol.proj.transform([136.819261,-31.165639], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var CentreInterarmeesEssaisEnginsSpeciaux = new ol.geom.Circle(
    // 4 lancés 0,1
    ol.proj.transform([1.444209,43.604652], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var CosmodromeDeSvobodny = new ol.geom.Circle(
    // 5 lancés 0,1
    ol.proj.transform([128.134722,51.376866], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var SpaceportFlorida = new ol.geom.Circle(
    // 2 lancés 0,1
    ol.proj.transform([-80.670118,28.514556 ], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var PacificSpaceportComplexAlaska = new ol.geom.Circle(
    // 3 lancés 0,1
    ol.proj.transform([-152.348359,57.436779], 'EPSG:4326', 'EPSG:3857'),
    100000
);
var MerDeBarents = new ol.geom.Circle(
    // 3 lancés 0,1
    ol.proj.transform([37.106368,74.988392], 'EPSG:4326', 'EPSG:3857'),
    100000
);
// Features
var CapeCanaveralAirStationFeature = new ol.Feature(CapeCanaveralAirStation);
var CosmodromeDeBaikonourFeature = new ol.Feature(CosmodromeDeBaikonour);
var VandenbergAirForceBaseFeature = new ol.Feature(VandenbergAirForceBaseFeature);
var CosmodromeDePlesetskFeature = new ol.Feature(CosmodromeDePlesetsk);
var SanMarcoPlateFormFeature = new ol.Feature(SanMarcoPlateForm);
var JiuquanSpaceCenterFeature = new ol.Feature(JiuquanSpaceCenter);
var XichangSpaceCenterFeature = new ol.Feature(XichangSpaceCenter);
var SatishDhawanSpaceCenterFeature = new ol.Feature(SatishDhawanSpaceCenter);
var KennedySpaceCenterFeature = new ol.Feature(KennedySpaceCenter);
var CentreSpatialGuyanaisFeature = new ol.Feature(CentreSpatialGuyanais);
var CosmodromeDeKapustinYarFeature = new ol.Feature(CosmodromeDeKapustinYar);
var TaiyuanSatelliteLaunchCenterFeature = new ol.Feature(TaiyuanSatelliteLaunchCenter);
var OceanPacifiqueFeature = new ol.Feature(OceanPacifique);
var UchinouraSpaceCenterFeature = new ol.Feature(UchinouraSpaceCenter);
var WallopsFlightFacilityFeature = new ol.Feature(WallopsFlightFacility);
var TanegashimaSpaceCenterFeature = new ol.Feature(TanegashimaSpaceCenter);
var CentroDeLacamentoDeAlcantaraFeature = new ol.Feature(CentroDeLacamentoDeAlcantara);
var MidAtlanticRegionalSpaceportFeature = new ol.Feature(MidAtlanticRegionalSpaceport);
var NaroSpaceCenterFeature = new ol.Feature(NaroSpaceCenter);
var CosmodromeDeDombarovskyFeature = new ol.Feature(CosmodromeDeDombarovsky);
var GandoAirBaseFeature = new ol.Feature(GandoAirBase);
var PalmachimAirBaseFeature = new ol.Feature(PalmachimAirBase);
var EdwardsAirForceBaseFeature = new ol.Feature(EdwardsAirForceBase);
var TonghaeSatelliteLaunchingGroundFeature = new ol.Feature(TonghaeSatelliteLaunchingGround);
var RonaldReaganBallisticMissileDefenseTestSiteFeature = new ol.Feature(RonaldReaganBallisticMissileDefenseTestSite);
var ChinaLakeAirForceBaseFeature = new ol.Feature(ChinaLakeAirForceBase);
var WoomeraRocketRangeFeature = new ol.Feature(WoomeraRocketRange);
var CentreInterarmeesEssaisEnginsSpeciauxFeature = new ol.Feature(CentreInterarmeesEssaisEnginsSpeciaux);
var CosmodromeDeSvobodnyFeature = new ol.Feature(CosmodromeDeSvobodny);
var SpaceportFloridaFeature = new ol.Feature(SpaceportFlorida);
var PacificSpaceportComplexAlaskaFeature = new ol.Feature(PacificSpaceportComplexAlaska);
var MerDeBarentsFeature = new ol.Feature(MerDeBarents);

// Source and vector layer
var vectorSource = new ol.source.Vector({
    projection: 'EPSG:4326',
    features: [CapeCanaveralAirStationFeature, CosmodromeDeBaikonourFeature, VandenbergAirForceBaseFeature, CosmodromeDePlesetskFeature, SanMarcoPlateFormFeature, JiuquanSpaceCenterFeature, XichangSpaceCenterFeature, SatishDhawanSpaceCenterFeature, KennedySpaceCenterFeature, CentreSpatialGuyanaisFeature, CosmodromeDeKapustinYarFeature, TaiyuanSatelliteLaunchCenterFeature, OceanPacifiqueFeature,  UchinouraSpaceCenterFeature, WallopsFlightFacilityFeature, TanegashimaSpaceCenterFeature, CentroDeLacamentoDeAlcantaraFeature, MidAtlanticRegionalSpaceportFeature, NaroSpaceCenterFeature, CosmodromeDeDombarovskyFeature, GandoAirBaseFeature, PalmachimAirBaseFeature, EdwardsAirForceBaseFeature, TonghaeSatelliteLaunchingGroundFeature, RonaldReaganBallisticMissileDefenseTestSiteFeature, ChinaLakeAirForceBaseFeature, WoomeraRocketRangeFeature, CentreInterarmeesEssaisEnginsSpeciauxFeature, CosmodromeDeSvobodnyFeature, SpaceportFloridaFeature, PacificSpaceportComplexAlaskaFeature, MerDeBarentsFeature]
});

var style = new ol.style.Style({
    fill: new ol.style.Fill({
        color: 'rgba(255, 100, 50, 0.3)'
    }),
    stroke: new ol.style.Stroke({
        width: 2,
        color: 'rgba(255, 100, 50, 0.8)'
    }),
    image: new ol.style.Circle({
        fill: new ol.style.Fill({
            color: 'rgba(55, 200, 150, 0.5)'
        }),
        stroke: new ol.style.Stroke({
            width: 1,
            color: 'rgba(55, 200, 150, 0.8)'
        }),
        radius: 7
    }),
});

var vectorLayer = new ol.layer.Vector({
    source: vectorSource,
    style: style
});

var map = new ol.Map({
    target: 'map',  // The DOM element that will contains the map
    renderer: 'canvas', // Force the renderer to be used
    layers: [
        // Add a new Tile layer getting tiles from OpenStreetMap source
        new ol.layer.Tile({
            source: new ol.source.OSM()
        }),
        vectorLayer
    ],
    // Create a view centered on the specified location and zoom level
    view: new ol.View({
        center: ol.proj.transform([4.922661, 45.782029], 'EPSG:4326', 'EPSG:3857'),
        zoom: 2
    })
});

