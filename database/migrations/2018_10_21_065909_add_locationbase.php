<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationbase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locationbase', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country');
            $table->string('name');
            $table->string('operational');
            $table->string('coordinates');
            $table->integer('launchCount');
            $table->string('altitude');
            $table->string('lat');
            $table->string('lng');
            $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locationbase');
    }
}
