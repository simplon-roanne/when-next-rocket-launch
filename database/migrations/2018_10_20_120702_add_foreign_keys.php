<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('missions', function (Blueprint $table) {
            $table->foreign('rocket_id')
                ->references('id')->on('rockets')
                ->onDelete('cascade');

            $table->foreign('launching_site_id')
                ->references('id')->on('sites')
                ->onDelete('cascade');

            $table->foreign('landing_site_id')
                ->references('id')->on('sites')
                ->onDelete('cascade');
        });

        Schema::table('payloads', function (Blueprint $table) {
            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');

            $table->foreign('mission_id')
                ->references('id')->on('missions')
                ->onDelete('cascade');
        });

        Schema::table('rockets', function (Blueprint $table) {
            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');
        });

        Schema::table('sites', function (Blueprint $table) {
            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
