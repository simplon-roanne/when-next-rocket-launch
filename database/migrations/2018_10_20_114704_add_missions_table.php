<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('type')->nullable();
            $table->string('status')->nullable();
            $table->string('flight_number')->nullable();
            $table->string('pas_de_tir')->nullable();
            $table->string('remarks')->nullable();
            $table->string('video')->nullable();
            $table->dateTime('launching_at')->nullable();
            $table->dateTime('landing_at')->nullable();
            $table->integer('rocket_id')->unsigned()->nullable();
            $table->integer('launching_site_id')->unsigned()->nullable();
            $table->integer('landing_site_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('missions');
    }
}
