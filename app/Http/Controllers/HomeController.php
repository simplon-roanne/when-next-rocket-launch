<?php

namespace App\Http\Controllers;

use App\Mission;

class HomeController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function scatter()
    {
        $annees = [];
        $nombrelance = [];
        for ($i = 1957; $i <= 2018; $i++) {
            array_push($annees, (string)$i);

        }

        //$missions = Mission::where('launching_at', '!=' ,NULL)->orderBy('launching_at', 'asc')->get();
        foreach ($annees as $annee) {
            // dd(Mission::where('launching_at', 'like', $annee.'%')->count());
            $tmp = Mission::where('launching_at', 'like', $annee . '%')->count();
            $nombrelance[$annee] = $tmp;
        }
        // dd($nombrelance);

        $points = [];

        foreach ($annees as $annee) {
            $points[] = [
                "x" => $annee,//$mission->launching_at->format('Y'),
                "y" => $nombrelance[$annee],
            ];
        }

        $points = json_encode($points);

        return view('scatter', compact('mission', 'points'));
    }

    public function timeline()
    {
        $missions = Mission::with('site', 'rocket', 'payloads')->limit(20)->orderBy('launching_at', 'desc')->get();

        return view('timeline', compact('missions'));
    }

}