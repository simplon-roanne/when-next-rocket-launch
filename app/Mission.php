<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mission extends Model
{
    protected $dates = ["created_at","updated_at","launching_at"];

    public function site()
    {
        return $this->belongsTo(Site::class, 'launching_site_id', 'id');
    }
    public function Rocket()
    {
        return $this->belongsTo(Rocket::class);
    }

    public function payloads()
    {
        return $this->hasMany(Payload::class);
    }


}
